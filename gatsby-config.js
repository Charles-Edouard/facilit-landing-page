module.exports = {
  siteMetadata: {
    title: `Facilitez la gestion de votre entreprise et de votre informatique`,
    description: `Facilit Gestion, solution web tout en un pour la gestion des TPE, Startup.Facilit Gestion vous accompagne dans la numérisation
    de votre entreprise. Des packs adaptés à vos besoins, comptabilité,caisse certifié,gestion de stock, RH,cloud`,
    author: `Facilit gestion`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-styled-components`,
    {
      resolve: `gatsby-plugin-prefetch-google-fonts`,
      options: {
        fonts: [
          {
            family: `Poppins`,
            variants: [`300`, `400`, `600`, `700`],
          },
        ],
      },
    },

    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `product`,
        path: `${__dirname}/src/images/product`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Facilit Gestion`,
        short_name: `Facilit Gestion`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/favicon.png`,
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    `gatsby-plugin-offline`,
  ],
}
