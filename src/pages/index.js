import React from "react"

import Layout from "../components/common/layout/layout"
import SEO from "../components/common/layout/seo"
import Navigation from "../components/common/navigation/navigation"

import Header from "../components/sections/header"
import Features from "../components/sections/features"
import Footer from "../components/sections/footer"
import GetStarted from "../components/sections/getstarted"
import GetPrice from "../components/sections/get_price"
import GetExplain from "../components/sections/getexplain"
import FeaturesElearning from "../components/sections/featureselearning"

const IndexPage = () => (
  <Layout>
    <SEO title="Facilit Gestion | Le meilleur des outils pour gérer la gestion de votre entreprise" description="Facilit Gestion | ERP | CRM | Cloud | Télétravail"/>
    <Navigation />
    <Header />
    <GetExplain />
    <Features />
    <GetStarted />
    <FeaturesElearning />
    <GetPrice />
    <Footer />
  </Layout>
)

export default IndexPage
