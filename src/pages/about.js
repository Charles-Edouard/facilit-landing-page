import React from "react"

import Layout from "../components/common/layout/layout"
import SEO from "../components/common/layout/seo"
import Navigation from "../components/common/navigation/navigation"

import Header from "../components/sections/header_about"
import Features from "../components/sections/features"
import Values from "../components/sections/values"
import Footer from "../components/sections/footer"
import GetStarted from "../components/sections/getstarted"
import GetExplain from "../components/sections/getexplain_about"
import FeaturesElearning from "../components/sections/featureselearning"

const IndexPage = () => (
  <Layout>
    <SEO title="Facilit Gestion" description="Facilit Gestion, c'est une équipe de passionnée et humaine. Nous avons créé le meilleur ecosystème de gestion"/>
    <Navigation />
    <Header />
    <GetExplain />
    <Values />
    <Features />
    <GetStarted />
    <FeaturesElearning />
    <Footer />
  </Layout>
)

export default IndexPage
