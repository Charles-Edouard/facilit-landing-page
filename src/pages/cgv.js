import React from "react"

import Layout from "../components/common/layout/layout"
import SEO from "../components/common/layout/seo"

import Footer from "../components/sections/footer"
import GetCGV from "../components/sections/getcgv"

const IndexPage = () => (
  <Layout>
    <SEO title="Facilit Gestion, nos condigtions générales de ventes" />
    <GetCGV />
    <Footer />
  </Layout>
)

export default IndexPage
