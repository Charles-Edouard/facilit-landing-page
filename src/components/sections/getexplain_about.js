import React from "react"
import styled from "styled-components"
import { Link } from "gatsby"

import { Container, Section } from "../global"

const GetExplain = () => (
  <StyledSection id="histoire">
    <GetExplainContainer>
      <GetExplainTitle>L'histoire du projet Facilit Gestion.</GetExplainTitle>
      <SubParagraphe>
      Facilit Gestion est née pour vous les petites entreprises, un service créé pour vous et par vous. Partant d'un constat, nous avons remarqué que les petites entreprises, les commerçants, les artisans n'avaient pas forcément le temps et la connaissance pour s'équiper correctement en informatique pour leurs activités. Facilit Gestion, c'est un sytème complet et simple pour votre entreprise ! Nous vous invitons à regarder l'histoire de Cécil.
      </SubParagraphe>
      <iframe frameborder="0" width="560" height="315" src="https://biteable.com/watch/embed/facilit-gestion-2501724" allowfullscreen="true" allow="autoplay"></iframe>
      <br></br>
      <TryItButton>
          <a href="https://platform.facilitgestion.com/testez-facilit-gestion">CURIEUX ?</a>
      </TryItButton>     
    </GetExplainContainer>
  </StyledSection>
)

export default GetExplain

const StyledSection = styled(Section)`
  background-color: ${props => props.theme.color.background.light};
  clip-path: polygon(0 0, 100% 14%, 100% 100%, 0% 100%);
`

const GetExplainContainer = styled(Container)`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  padding: 80px 0 40px;
`

const GetExplainTitle = styled.h3`
  margin: 0 auto 32px;
  text-align: center;
`

const TryItButton = styled.button`
  font-weight: 500;
  font-size: 14px;
  color: white;
  letter-spacing: 1px;
  height: 60px;
  display: block;
  margin-left: 8px;
  text-transform: uppercase;
  cursor: pointer;
  white-space: nowrap;
  background: ${props => props.theme.color.secondary};
  border-radius: 4px;
  padding: 0px 40px;
  border-width: 0px;
  border-style: initial;
  border-color: initial;
  border-image: initial;
  outline: 0px;
  &:hover {
    box-shadow: rgba(110, 120, 152, 0.22) 0px 2px 10px 0px;
  }
  @media (max-width: ${props => props.theme.screen.md}) {
  }
  @media (max-width: ${props => props.theme.screen.sm}) {
    margin-left: 0;
  }
`

const SubParagraphe = styled.span`
  ${props => props.theme.font_size.xxsmall}
  padding-top: 16px;
  margin-bottom: 25px;
  text-align: center;
  font-size: 18px;
  color: ${props => props.theme.color.primary};
  `

const Subtitle = styled.span`
    ${props => props.theme.font_size.xxsmall}
    padding-top: 16px;
    font-size: 14px;
    color: ${props => props.theme.color.primary};
`
