import React from "react"
import styled from "styled-components"
import { Link } from "gatsby"

import { Container, Section } from "../global"

const GetExplain = () => (
  <StyledSection id="histoire">
    <GetExplainContainer>
      <GetExplainTitle>Qu'est ce que Facilit Gestion ?</GetExplainTitle>
      <SubParagraphe>
          Facilit gestion s'adresse aux TPE,TPI et micro-entreprises qui souhaite trouver rapidement l'outil informatique adapté à leurs besoins.
            C'est une plateforme qui regroupe un ensemble de logiciel et application web open source pour gérer votre entreprise. 
            Ce n'est pas uniquement ça, nous déployons pour vous et hébergeons pour vous vos données tout en vous laissant la maîtrise de celle-ci. 
            Et aussi nous vous accompagnons dans leurs usages grâce à notre plateforme E-Learning.
      </SubParagraphe>
      <iframe frameborder="0" width="560" height="315" src="https://biteable.com/watch/embed/consultancy-ad-copy-2497895" allowfullscreen="true" allow="autoplay"></iframe>
      <br></br>
      <TryItButton>
        <Link to="/about">Découvrir notre histoire</Link>
      </TryItButton>     
    </GetExplainContainer>
  </StyledSection>
)

export default GetExplain

const StyledSection = styled(Section)`
  background-color: ${props => props.theme.color.background.light};
  clip-path: polygon(0 0, 100% 14%, 100% 100%, 0% 100%);
`

const GetExplainContainer = styled(Container)`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  padding: 80px 0 40px;
`

const GetExplainTitle = styled.h3`
  margin: 0 auto 32px;
  text-align: center;
`

const TryItButton = styled.button`
  font-weight: 500;
  font-size: 14px;
  color: white;
  letter-spacing: 1px;
  height: 60px;
  display: block;
  margin-left: 8px;
  text-transform: uppercase;
  cursor: pointer;
  white-space: nowrap;
  background: ${props => props.theme.color.secondary};
  border-radius: 4px;
  padding: 0px 40px;
  border-width: 0px;
  border-style: initial;
  border-color: initial;
  border-image: initial;
  outline: 0px;
  &:hover {
    box-shadow: rgba(110, 120, 152, 0.22) 0px 2px 10px 0px;
  }
  @media (max-width: ${props => props.theme.screen.md}) {
  }
  @media (max-width: ${props => props.theme.screen.sm}) {
    margin-left: 0;
  }
`

const SubParagraphe = styled.span`
  ${props => props.theme.font_size.xxsmall}
  padding-top: 16px;
  margin-bottom: 25px;
  text-align: center;
  font-size: 18px;
  color: ${props => props.theme.color.primary};
  `

const Subtitle = styled.span`
    ${props => props.theme.font_size.xxsmall}
    padding-top: 16px;
    font-size: 14px;
    color: ${props => props.theme.color.primary};
`
