import React from "react"
import styled from "styled-components"

import { Container, Section } from "../global"

const GetStarted = () => (
  <StyledSection id="essai">
    <GetStartedContainer>
    <SubParagraphe>
    <h1>CONDITIONS GENERALES DE VENTE</h1>
    <h2>Clause n° 1 : Objet</h2>
        Les conditions générales de vente décrites ci-après détaillent les droits et
        obligations de la société FacilitGestion SAS et de son client dans le cadre
        de la vente des marchandises suivantes : plateforme de gestion
        d’entreprise FacilitGestion SAS.
        Toute prestation accomplie par la société FacilitGestion SAS implique donc
        l&#39;adhésion sans réserve de l&#39;acheteur aux présentes conditions générales
        de vente.
    <h2>Clause n° 2 : Prix</h2>
        Les prix des marchandises vendues sont ceux en vigueur au jour de la
        prise de commande. Ils sont libellés en euros et calculés hors taxes. Par
        voie de conséquence, ils seront majorés du taux de TVA et des frais de
        transport applicables au jour de la commande.
        La société FacilitGestion SAS s&#39;accorde le droit de modifier ses tarifs à tout
        moment. Toutefois, elle s&#39;engage à facturer les marchandises commandées
        aux prix indiqués lors de l&#39;enregistrement de la commande.
    <h2>Clause n° 3 : Rabais et ristournes</h2>
        Les tarifs proposés comprennent les rabais et ristournes que la société
        FacilitGestion SAS serait amenée à octroyer compte tenu de ses résultats
        ou de la prise en charge par l&#39;acheteur de certaines prestations.
    <h2>Clause n° 4 : Escompte</h2>
        Aucun escompte ne sera consenti en cas de paiement anticipé.
    <h2>Clause n° 5 : Modalités de paiement</h2>
        Le règlement des commandes s&#39;effectue :
         soit par prélèvement bancaire ;
         soit par carte bancaire ;
        Lors de l&#39;enregistrement de la commande, l&#39;acheteur devra verser un
        acompte de 100% du montant global de la facture.
    <h2>Clause n° 6 : Retard de paiement</h2>
        Fait à Carentan Les Marais, le 01 mars 2020
        En cas de défaut de paiement total ou partiel des marchandises livrées au
        jour de la réception, l&#39;acheteur doit verser à la société FacilitGestion SAS
        une pénalité de retard égale à trois fois le taux de l&#39;intérêt légal.
        Le taux de l&#39;intérêt légal retenu est celui en vigueur au jour de la livraison
        des marchandises.
        Cette pénalité est calculée sur le montant TTC de la somme restant due, et
        court à compter de la date d&#39;échéance du prix sans qu&#39;aucune mise en
        demeure préalable ne soit nécessaire.
        En sus des indemnités de retard, toute somme, y compris l’acompte, non
        payée à sa date d’exigibilité produira de plein droit le paiement d’une
        indemnité forfaitaire de 40 euros due au titre des frais de recouvrement.
        Articles 441-6, I alinéa 12 et D. 441-5 du code de commerce.
    <h2>Clause n° 7 : Clause résolutoire</h2>
        Si dans les quinze jours qui suivent la mise en œuvre de la clause &quot; Retard
        de paiement &quot;, l&#39;acheteur ne s&#39;est pas acquitté des sommes restantes dues,
        la vente sera résolue de plein droit et pourra ouvrir droit à l&#39;allocation de
        dommages et intérêts au profit de la société FacilitGestion SAS.
    <h2>Clause n° 8 : Livraison</h2>
        La livraison est effectuée :
         Accès aux outils de gestions le jour même du paiement de la facture.
    <h2>Clause n° 9 : Force majeure</h2>
        La responsabilité de la société FacilitGestion SAS ne pourra pas être mise
        en œuvre si la non-exécution ou le retard dans l&#39;exécution de l&#39;une de ses
        obligations décrites dans les présentes conditions générales de vente
        découle d&#39;un cas de force majeure. À ce titre, la force majeure s&#39;entend de
        tout événement extérieur, imprévisible et irrésistible au sens de l&#39;article
        1148 du Code civil.
    <h2>Clause n° 10 : Tribunal compétent</h2>
        Tout litige relatif à l&#39;interprétation et à l&#39;exécution des présentes conditions
        générales de vente est soumis au droit français.
        À défaut de résolution amiable, le litige sera porté devant le Tribunal de
        commerce de Cherbourg, 22 Rue de l&#39;Ancien Quai, 50100 Cherbourg-en-
            </SubParagraphe>
    </GetStartedContainer>
  </StyledSection>
)

export default GetStarted

const StyledSection = styled(Section)`
  background-color: ${props => props.theme.color.background.light};
  clip-path: polygon(0 0, 100% 14%, 100% 100%, 0% 100%);
`

const GetStartedContainer = styled(Container)`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  padding: 80px 0 40px;
`

const SubParagraphe = styled.span`
  ${props => props.theme.font_size.xxsmall}
  padding-top: 16px;
  margin-bottom: 25px;
  text-align: center;
  font-size: 18px;
  color: ${props => props.theme.color.primary};
`