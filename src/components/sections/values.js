import React from "react"
import styled from "styled-components"

import { Section, Container } from "../global"

const Features = () => (
  <Section id="values">
    <StyledContainer>
      <Subtitle>Des valeurs qui nous tiennent à coeur</Subtitle>
      <SectionTitle>Facilit Gestion ce n'est pas seulement vous facilitez la vie.</SectionTitle>
      <FeaturesGrid>
        <FeatureItem>
          <FeatureTitle>Enchantement client</FeatureTitle>
          <FeatureText>
            Ça ne veut rien dire ? Pas pour nous, vous satisfaire et vous facilitez la vie et notre priorité absolue. Pour celà nous mettons tout en oeuvre pour vous offrir un service simple et complet.
          </FeatureText>
        </FeatureItem>
        <FeatureItem>
          <FeatureTitle>Ethique</FeatureTitle>
          <FeatureText>
          C'est quoi l'Ethique informatique ? Nous adoptons un maximum une démarche Green IT en limitant la consommation, et aussi afin de vous garantir la maîtrise de vos données nous n'utilisons que des applications libres et open sources.
          </FeatureText>
        </FeatureItem>
        <FeatureItem>
          <FeatureTitle>Engagement</FeatureTitle>
          <FeatureText>
            Engager dans quoi ? Nous souhaitons réduire la fracture numérique des très petites entreprises en vous équipant avec les moyens informatiques des grandes entreprises.
          </FeatureText>
        </FeatureItem>
        <FeatureItem>
          <FeatureTitle>Relationnelle</FeatureTitle>
          <FeatureText>
            Vous avez l'habitude de la proximité ? Ça tombe bien ! Nous ne vendons notre service uniquement par le biais de revendeur qui sont des magasins physiques agréés par Facilit Gestion.
          </FeatureText>
        </FeatureItem>
        <FeatureItem>
          <FeatureTitle>Conquête</FeatureTitle>
          <FeatureText>
            Il est important pour nous de "grossir" afin de maintenir une qualité de service et de toujours proposer des applications totalement interconnectés et viables.
          </FeatureText>
        </FeatureItem>
      </FeaturesGrid>
    </StyledContainer>
  </Section>
)

export default Features

const StyledContainer = styled(Container)``

const SectionTitle = styled.h3`
  color: ${props => props.theme.color.primary};
  display: flex;
  justify-content: center;
  margin: 0 auto 40px;
  text-align: center;
`

const Subtitle = styled.h5`
  font-size: 16px;
  color: ${props => props.theme.color.accent};
  letter-spacing: 0px;
  margin-bottom: 12px;
  text-align: center;
`

const FeaturesGrid = styled.div`
  max-width: 670px;
  display: grid;
  grid-template-columns: 1fr 1fr;
  margin: 0px auto;
  grid-column-gap: 40px;
  grid-row-gap: 35px;
  @media (max-width: ${props => props.theme.screen.sm}) {
    grid-template-columns: 1fr;
    padding: 0 64px;
  }
`

const FeatureItem = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`

const FeatureTitle = styled.h4`
  color: ${props => props.theme.color.primary};
  letter-spacing: 0px;
  line-height: 30px;
  margin-bottom: 10px;
`

const FeatureText = styled.p`
  text-align: center;
`
